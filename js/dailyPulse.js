let token = null;
const apiUrl = 'https://api.teammy.app/';
let userName = null;
let userDepartment = null;

//logout
$(document).on('click', '#logout', function () {
    chrome.storage.sync.clear();
    window.location.href = "login.html";
});

$(() => {

    //get user token
    chrome.storage.sync.get(['token', 'userName', 'userDepartment'], function (result) {
        if (result.token) {
            token = result.token;
            //set user name and department
            userName = result.userName;
            userDepartment = result.userDepartment;
            $('#user-name').text(result.userName);
            $('#user-department').text(result.userDepartment);
            $('.user-name').text(userName);
            getSelectedStars(result.token)
        }
    });
    //in general like/dislike
    $('#daily-pulse-like').on('click', function () {
        $(this).addClass('thump-up-active');
        $('#daily-pulse-dislike').removeClass(' thump-up-active');
        dailyPulseLike = 1;
    });
    $('#daily-pulse-dislike').on('click', function () {
        $(this).addClass('thump-up-active');
        $('#daily-pulse-like').removeClass(' thump-up-active');
        dailyPulseLike = 0;
    });

    //send Daily Pulse
    $('#sendDailyPulse').on('click', checkDailyPulseData);
});



//data
let dailyPulseLike = null;
let myPerformance = null;
let teamsPerformance = null;
let teamProgress = null;
let teamAtmosphere = null;
let dinnerQuality = null;
let officeCleanliness = null;
let managementDecisions = null;


function checkDailyPulseData(){
    let likesErrors = '';
    let starsErrors = '';
    if (dailyPulseLike === null) {
        likesErrors += 'Please select like or dislike'
    }
    if (
        myPerformance === null ||
        teamsPerformance === null ||
        teamProgress === null ||
        teamAtmosphere === null ||
        dinnerQuality === null ||
        officeCleanliness === null ||
        managementDecisions === null
    ) {
        starsErrors += 'Please select star rate for each list item'
    }
    $('#daily-pulse-likes-errors').text(likesErrors);
    $('#daily-pulse-stars-errors').text(starsErrors);
    if (likesErrors === '' && starsErrors === '') sendDailyPulseData(likesErrors, starsErrors);
}

function sendDailyPulseData(likesErrors, starsErrors){
    const url = apiUrl + "api/daily-pulse";
    let data = {
        in_general: dailyPulseLike,
        my_performance: myPerformance,
        teams_performance: teamsPerformance,
        team_progress: teamProgress,
        team_atmosphere: teamAtmosphere,
        dinner_quality: dinnerQuality,
        office_cleanliness: officeCleanliness,
        management_decisions: managementDecisions
    };

    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        headers: {"Authorization": 'Bearer ' + token},
        dataType: "json",
        contentType: "application/json",
        success: function (jsonData) {
            removeSendDailyPulse()
        },
        error: function (e) {
            dailyPulseErrorMessage()
        }
    })
}

function dailyPulseErrorMessage(){
    $('#daily-pulse-error-message')
        .removeClass('d-none')
        .addClass('d-flex');
}

//mounted
function getSelectedStars(token) {
    let selectedStars = null;

    const getStarsUrl = apiUrl + 'api/daily-pulse';
    let options = {
        stars: 5,
        color: '#ffc11e',
        click: null
    };

    $.ajax({
        type: "GET",
        url: getStarsUrl,
        headers: {"Authorization": 'Bearer ' + token},
        dataType: "json",
        contentType: "application/json",
        success: function (jsonData) {
            // Success callback
            selectedStars = jsonData
        },
        error: function (e) {
            console.error(e)
        }
    }).then(() => {
        //disabling send button
        //got string message in response if it will be first record in db of this day
        if (typeof selectedStars !== 'string') {
            removeSendDailyPulse()
        }
        // in general like
        dailyPulseLike = selectedStars.in_general || selectedStars.in_general === 0 ? selectedStars.in_general : null;
        if (dailyPulseLike === 1) {
            $('#daily-pulse-like').addClass('thump-up-active')
        } else if (dailyPulseLike === 0) {
            $('#daily-pulse-dislike').addClass('thump-up-active')
        }
        //stars plugin
        options.click = mpFunc;
        options.value = selectedStars && selectedStars.my_performance ? selectedStars.my_performance : 0;
        $('#my-performance-stars').stars(options);

        options.click = tprFunc;
        options.value = selectedStars && selectedStars.teams_performance ? selectedStars.teams_performance : 0;
        $('#teams-performance-stars').stars(options);

        options.click = tpFunc;
        options.value = selectedStars && selectedStars.team_progress ? selectedStars.team_progress : 0;
        $('#team-progress').stars(options);

        options.click = taFunc;
        options.value = selectedStars && selectedStars.team_atmosphere ? selectedStars.team_atmosphere : 0;
        $('#team-atmosphere').stars(options);

        options.click = dqFunc;
        options.value = selectedStars && selectedStars.dinner_quality ? selectedStars.dinner_quality : 0;
        $('#dinner-quality').stars(options);

        options.click = ocFunc;
        options.value = selectedStars && selectedStars.office_cleanliness ? selectedStars.office_cleanliness : 0;
        $('#office-cleanliness').stars(options);

        options.click = mdFunc;
        options.value = selectedStars && selectedStars.management_decisions ? selectedStars.management_decisions : 0;
        $('#management-decisions').stars(options);
    });
}

//helper function
function removeSendDailyPulse() {
    $('.sendDailyPulse').remove();
    $('#daily-pulse-success-message')
        .removeClass('d-none')
        .addClass('d-flex');
    $('#daily-pulse-error-message')
        .removeClass('d-flex')
        .addClass('d-none');

}

//get stars value
function mpFunc(index) {
    myPerformance = index;
}

function tprFunc(index) {
    teamsPerformance = index;
}

function tpFunc(index) {
    teamProgress = index;
}

function taFunc(index) {
    teamAtmosphere = index;
}

function dqFunc(index) {
    dinnerQuality = index;
}

function ocFunc(index) {
    officeCleanliness = index;
}

function mdFunc(index) {
    managementDecisions = index;
}
