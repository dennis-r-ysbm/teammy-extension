let url = "https://api.teammy.app/api/";

let requestType = 'login';

$(() => {
    showPopup();
});

$(document).on('click', '#login-tab', function () {
    requestType = 'login'
});
$(document).on('click', '#register-tab', function () {
    requestType = 'register'
});
$(document).on('click', '#auth', function () {
    let data = {
        email: $('#email').val(),
        password: $('#password').val()
    };

    let action = 'login';
    if (requestType === 'register') {
        data.retry_password = $('#retry-password').val();
        data.name = $('#name').val();
        action = 'register';
    }
    //reset token and user data
    resetToken();
    $.ajax({
        type: "POST",
        url: url + action,
        data: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        dataType: "json",
        contentType: "application/json",
        success: function (jsonData) {
            // Success callback
            setToken(jsonData.token, jsonData.name, jsonData.department)
            showPopup()
        },
        error: function (e) {
            if (action === 'login') {
                loginErrorMessage(e.responseJSON.error)
            } else {
                registerErrorMessage(e.responseJSON.error)
            }
        }
    });
});
//clear error message
$(document).on('click', '#login-tab', function () {
    $('#register-error-message')
        .removeClass('d-flex')
        .addClass('d-none');
});
$(document).on('click', '#register-tab', function () {
    $('#login-error-message')
        .removeClass('d-flex')
        .addClass('d-none');
});

function loginErrorMessage(errorMessage) {
    $('#login-error-message')
        .text(errorMessage)
        .removeClass('d-none')
        .addClass('d-flex');
}

function registerErrorMessage(errorMessage) {
    $('#register-error-message')
        .text(errorMessage)
        .removeClass('d-none')
        .addClass('d-flex');
}

//
function setToken(token, name, department) {
    chrome.storage.sync.set({
        token: token,
        userName: name,
        userDepartment: department
    })
}
function resetToken() {
    chrome.storage.sync.clear()
}

function showPopup() {
    chrome.storage.sync.get('token', function (result) {
        if (result.token) {
            window.location.href = "main.html";
        } else if (window.location.pathname !== "/login.html") {
            window.location.href = "login.html";
        }
    });
}

