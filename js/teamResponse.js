// token variable was inited in dailyPulse.js
let teamResponseMessage = null;
let teamResponseData = {
    message: '',
    type: '',
    urgency: null,
};
const options = {
    stars: 5,
    color: '#ffc11e',
    value: 0,
    click: function (index) {
        teamResponseData.urgency = index;
    }
};

$(() => {
    chrome.storage.sync.get('token', function (result) {
        if (result.token) {
            token = result.token;
        }
    });
    //message
    teamResponseMessage = $('#team-response-message').val();
    //urgency
    $('#team-response-stars').stars(options);

});
//select message type


$(document).on('click', '.response-type', function () {
    $('.response-type.active').removeClass('active');
    $(this).addClass('active');
    teamResponseData.type = $(this).data('type');
});

$(document).on('input', '#team-response-message', function (e) {
    teamResponseData.message = e.target.value;
    //remove successful message
    if (teamResponseData.message !== '') {
        $('#team-response-success-message')
            .removeClass('d-flex')
            .addClass('d-none');
    }
});

$(document).on('click', '#send-team-response', function (e) {
    let errors = '';
    const url = apiUrl + 'api/team-response';
    if (teamResponseData.message === '') errors += 'Please enter message. ';
    if (teamResponseData.type === '') errors += 'Please select message type. ';
    if (teamResponseData.urgency === null) errors += 'Please select stars rate. ';
    $('#send-team-response-errors').text(errors);

    if (errors === '') {
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(teamResponseData),
            headers: {"Authorization": 'Bearer ' + token},
            dataType: "json",
            contentType: "application/json",
            success: function (jsonData) {
                teamResponseSuccessfulMessage()
            },
            error: function (e) {
                teamResponseErrorMessage()
            }
        })
    } else {
        $('#team-response-error-message')
            .removeClass('d-flex')
            .addClass('d-none');
    }
});

function teamResponseSuccessfulMessage() {
    $('#team-response-success-message')
        .removeClass('d-none')
        .addClass('d-flex m-auto text-success');
    teamResponseData = {
        message: '',
        type: '',
        urgency: null,
    };
    $('.response-type.active').removeClass('active');
    $('#team-response-message').val('');
    $('#team-response-stars').empty().stars(options)
    $('#team-response-error-message')
        .removeClass('d-flex')
        .addClass('d-none');
}

function teamResponseErrorMessage() {
    $('#team-response-error-message')
        .removeClass('d-none')
        .addClass('d-flex');
}



