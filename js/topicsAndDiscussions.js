// token variable was inited in dailyPulse.js
let rateTopicOrDiscussion = {};

//get all topics and discussions
$(() => {
    chrome.storage.sync.get('token', function (result) {
        if (result.token) {
            token = result.token;
            getTopicsAndDiscussions(token)
        }
    });
});

// send topic or discussion
$(document).on('click', '.send-topic-or-discussion', function () {
    const url = apiUrl + "api/topic-and-discussions/";

    const id = $(this).data('topic-or-discussion-id');
    const parenEl = $(this).closest('.parent-topic-and-discussion');
    const message = parenEl.find('.message-topic-or-discussion').val();
    const errorBlock = parenEl.find('.errors-block');

    let data = {
        rate: rateTopicOrDiscussion[id],
        message: message
    };
    checkTopicOrDiscussion(url, id, data, token, parenEl, errorBlock);

});

function checkTopicOrDiscussion(url, id, data, token, parenEl, errorBlock){
    let errors = '';
    console.log(url, id, data, token);
    if (data.message === '') errors += 'Please enter message. ';
    if (data.rate === undefined) errors += 'Please select rate.';
    errorBlock.text(errors);
    if (errors === '') sendTopicOrDiscussion(url, id, data, token, parenEl)
}

function sendTopicOrDiscussion2(url, id, data, token){
    alert(url, id, data, token);
}

function sendTopicOrDiscussion(url, id, data, token, parenEl){
    $.ajax({
        type: "POST",
        url: url + id,
        data: JSON.stringify(data),
        headers: {"Authorization": 'Bearer ' + token},
        dataType: "json",
        contentType: "application/json",
        success: function (jsonData) {
            // Success callback
            sendTopicOrDiscussionSuccess(parenEl)
        },
        error: function (e) {
            sendTopicOrDiscussionError(parenEl);
        }
    });
}

function sendTopicOrDiscussionSuccess(parenEl){
/*    parenEl.find('.topics-and-discussions-server-errors-block')
        .removeClass('d-flex')
        .addClass('d-none');*/
    parenEl.remove();
    if ( $('#profile .parent-topic-and-discussion').length === 0 ) $('#profile').append('<div class="text-center mt-2">All topics and discussions are done</div>');
}

function sendTopicOrDiscussionError(parenEl){
    parenEl.find('.topics-and-discussions-server-errors-block')
        .removeClass('d-none')
        .addClass('d-flex');
}

//like topic
$(document).on('click', '.like-topic', function () {
    const id = $(this).data('topic-or-discussion-id');
    rateTopicOrDiscussion[id] = 1;
    $(this).addClass('thump-up-active');

    $('.dislike-topic[data-topic-or-discussion-id="' + id + '"]').removeClass('thump-up-active');
});
//dislike topic
$(document).on('click', '.dislike-topic', function () {
    const id = $(this).data('topic-or-discussion-id');
    rateTopicOrDiscussion[id] = null;
    $(this).addClass('thump-up-active');

    $('.like-topic[data-topic-or-discussion-id="' + id + '"]').removeClass('thump-up-active');
});

//helper functions
function getTopicsAndDiscussions(token) {
    const url = apiUrl + "api/topic-and-discussions";

    $.ajax({
        type: "GET",
        url: url,
        headers: {"Authorization": 'Bearer ' + token},
        dataType: "json",
        contentType: "application/json",
        success: function (jsonData) {
            // Success callback
            fillTopicsAndDiscussionsContent(jsonData)
        },
        error: function (e) {
            fillTopicsAndDiscussionsContentOnError(e)
        }
    })
}

function fillTopicsAndDiscussionsContentOnError(response) {
    $('#profile').append('<div class="text-center mt-2">Sorry, something went wrong</div>');
}

function fillTopicsAndDiscussionsContent(response) {
    let responseTemplate = '';
    if (response.length === 0) {
        $('#profile').append('<div class="text-center mt-2">All topics and discussions are done</div>');
    } else {
        response.forEach(value => {
            responseTemplate = createBaseElement(value.message, value.type, value.id);

            $('#profile').append(responseTemplate);

            //stars plugin
            const options = {
                stars: 5,
                color: '#ffc11e',
                click: function (index) {
                    rateTopicOrDiscussion[value.id] = index;
                }
            };
            $('.stars-discussion-' + value.id).stars(options);
        });
    }
}

function createBaseElement(message, type, id) {
    const baseStart = '<div class="bg-white mt-2 ml-3 mr-3 parent-topic-and-discussion">\n' +
        '                <div class="text-center text-lg font-semi-bold pt-1">\n' +
        '                    <span>';
    const baseMiddle = '</span>\n' +
        '                </div>\n' +
        '                <div class="row ml-3 mr-3 mt-1 mb-1">\n' +
        '                    <textarea class="form-control message-topic-or-discussion" rows="3" placeholder="Share your thoughts"></textarea>\n' +
        '                </div>\n';
    const likesBlock = '                <div class="row pt-1 pb-1">\n' +
        '                    <div class="w-50 text-center">\n' +
        `                        <div class="dislike-topic cursor-pointer rotate-180 m-auto thump-up" data-topic-or-discussion-id="${id}"></div>\n` +
        '                    </div>\n' +
        '                    <div class="w-50 text-center">\n' +
        `                        <div class="like-topic cursor-pointer m-auto thump-up" data-topic-or-discussion-id="${id}"></div>\n` +
        '                    </div>\n' +
        '                </div>\n';
    const starsBlock = `<div class="row stars-discussion-${id} pt-1 pb-1 justify-content-center">\n` +
        '                </div>\n';
    const errorsBlock = `<div class="errors-block text-center text-danger p-1 justify-content-center">\n` +
        '                </div>\n';
    const serverErrorsBlock = `<div class="topics-and-discussions-server-errors-block text-center text-danger p-1 justify-content-center d-none">\n` +
        'Something went wrong! Please try again later</div>\n';
    const sendButton = '<div class="row justify-content-center">\n' +
        `                    <button data-topic-or-discussion-id="${id}"\n` +
        '                            class="send-topic-or-discussion btn btn-warning rounded-pill mb-2"\n' +
        '                    >\n' +
        '                        Send\n' +
        '                    </button>\n' +
        '                </div>' +
        '             </div>';

    if (type === 'reaction') {
        return baseStart + message + baseMiddle + likesBlock + errorsBlock + serverErrorsBlock + sendButton
    } else if (type === 'rate') {
        return baseStart + message + baseMiddle + starsBlock + errorsBlock + serverErrorsBlock + sendButton
    }

}



